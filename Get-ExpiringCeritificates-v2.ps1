<#
.SYNOPSIS
    Retrieves information about expiring certificates and generates an email report
.DESCRIPTION
    This script will query a list of computers for certificates in their Local Machine 
    store or a Certificate Authority for issued certificates and generate a report to be
    sent via email on those that are nearing expiration.
.NOTES
    Author          : Josh Meyer
    Prerequisite    : PowerShell 5.0 or later
    License         : GPLv3
    Use at your own risk; not responsible for any damages!
#>
<#
.PARAMETER Computers
    A list of computer names to query for expiring certificates
.PARAMETER CertificateAuthority
    The host and CA name, in "hostname\CA Name" format, to query for
    expiring certificates
.PARAMETER Days
    An integer representing the number of days in the future to check for 
    expiring certificates
#>
[CmdletBinding(DefaultParameterSetName="Computers")]
Param (
    [parameter(Mandatory=$true,ParameterSetName="Computers",HelpMessage="String Array of Computer Names to query")]
    [string[]]$Computers,
    [parameter(Mandatory=$true,ParameterSetName="CertificateAuthority",HelpMessage="Name of the Certificate Authority to query")]
    [ValidatePattern("^\w+(['-._]\w+)*\\\w+(['-._]\w+)*")]
    [string]$CertificateAuthority,
    [parameter(HelpMessage="Number of days in the future to check")]
    [int]$Days
)
begin {
    # Global Variables
    $EMAIL_TO = [System.Net.Mail.MailAddressCollection]::new();
    $EMAIL_TO.Add("support@zephyruscomputing.com");
    $EMAIL_FROM = [System.Net.Mail.MailAddress]::new("cert-notify@zephyruscomputing.com","Certificate Expiration Notification Service");
    function Get-ExpiringComputerCertificates{
        param(
            [string]$ComputerName,
            [int]$Days
        )
        $certs = '';
        $future = (Get-Date).AddDays($Days);
        $flags = ([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadOnly,
            [System.Security.Cryptography.X509Certificates.OpenFlags]::MaxAllowed);
        $store = New-Object System.Security.Cryptography.X509Certificates.X509Store(
            ("\\{0}\my" -f $ComputerName), "LocalMachine");
        try {
            $store.Open($flags);
            $certs = $store.Certificates | Where-Object {$_.NotAfter -le $future } | Select-Object Issuer, Subject, Thumbprint, NotAfter;
            return $certs;
        } finally { $store.Close(); }
    }
    function Get-ExpiringCACertificates {
        param (
            [ValidatePattern("^\w+(['-._]\w+)*\\\w+(['-._]\w+)*")]
            [string]$CA,
            [int]$Days,
        )
        $certs = @{}
        $now = Get-Date
        $future = $now.AddDays($Days)
        try {
            $raw = certutil.exe -view -config "$CA" -restrict "-NotAfter>=$now,NotAfter<=$future" -out "CommonName,CertificateHash,NotAfter"
            [int]$count = ($raw -like "*Rows")[0].split(" ")[0]
            if ($count -gt 0){
                $index = $raw.IndexOf("Row 1") -1
                $schema = $raw[0..$index]
                for ($i = 1; $i -le $count; $i++){
                    $index = $raw.IndexOf("Row {0}:" -f $i) +1
                    if ($i -eq $count){
                        $end = $raw.IndexOf("{0} Rows" -f $i) -2
                    } else {
                        $end = $raw.IndexOf("Row {0}:" -f $($i + 1)) -1
                    }
                    $certdata = $raw[$index..$end]
                    $cert = Get-CertificatePSObject -Schema $schema -Data $certdata
                    $certs.add($i,$cert)
                }
            }
        }
        catch [System.Exception] {Write-Host $_ }
        return $certs
    }
    function Get-CertificatePSObject {
        param (
            [string[]]$Schema,
            [string[]]$Data
        )
        $reference = @{}
        $table = @{}
        foreach ($line in $Schema){
            if ($line.Trim() -match "^Schema"){ continue }
            if ($line.Trim() -match "^Column"){ continue }
            if ($line.Trim() -match "^---"){ continue }
            if ($line.Trim() -eq ""){ continue }
            $value = $line.Substring(2,28).Trim()
            $key = $line.Substring(32,28).Trim()
            $reference.add($key,$value)
        }
        foreach ($line in $Data){
            $key = $line.Trim().Split(":",2)
            if ($key[0] -in $reference.Keys){
                $table.Add($reference."$($key[0])",$key[1].Trim())
            }
        }
        return New-Object psobject -Property $table
    }
    function Format-Report{
        param(
            [hashtable]$Objects
        )
        $html = "<table><thead>"
        foreach ($header in $($Objects.Values|Get-Member -MemberType NoteProperty|Select Name).Name){
            $html += "<th>{0}</th>" -f $header
        }
        $html += "</thead><tbody>"
        if ($Objects.ContainsKey(1)){
            foreach ($cert in $Objects.Values){
                $html += "<tr>"
                foreach ($property in $($cert|Get-Member -MemberType NoteProperty|Select Name).Name){
                        $html += "<td>{0}</td>" -f $cert."$property"
                }
                $html += "</tr>"
            }
        } else {
            $properties = $($Objects.Values|Get-Member -MemberType NotePropert|Select Name).Name
            $colLen = $properties.Count
            foreach ($system in $Objects.Keys){
                $html += "<tr><td colspan='{0}'>{1}</td></tr>" -f $colLen, $system
                foreach ($row in $Objects[$system]){
                    $html += "<tr>"
                    foreach ($property in $properties){
                        $html += "<td>{0}</td>" -f $row."$property"
                    }
                    $html += "</tr>"
                }
            }
        }
        $html += "</tbody></table>"
        return $html
    }
    function Send-Email{
        param(
            $Subject,
            $Message,
            $To,
            $From
        )
        $email = New-Object System.Net.Mail.MailMessage;
        $email.IsBodyHtml = $true;
        $email.Subject = "$Subject";
        $email.Body = $Message;
        $email.From = $From;
        foreach ($address in $To){
            $email.To.Add($address);
        }
        $smtpClient = New-Object System.Net.Mail.SmtpClient("smtp.zephyruscomputing.com", 25);
        $smtpClient.UseDefaultCredentials = $false;
        $smtpClient.Send($email);
        return $?;
    }
}
process {
    $report = @"
<!DOCTYPE html>
<html xlmns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head><style>
<!-- Add your own CSS styling here -->
</style></head>
<body>
"@;
    $reportObjects = @{};
    if ($PsCmdlet.ParameterSetName -eq "Computers"){
        $subject = "Expiring Computer Certificates Report";
        foreach ($computer in $Computers){
            $certificates = Get-ExpiringComputerCertificates -ComputerName $computer -Days $Days;
            foreach ($certificate in $certificates){
                $reportObjects.Add($computer, $certificate);
            }
        }
    }
    if ($PsCmdlet.ParameterSetName -eq “CertificateAuthority”){
        $subject = "Expiring CA Certificates Report";
        $reportObjects = Get-ExpiringCACertificates -CA $CertificateAuthority -Days $Days;
    }
    $report += Format-Report -Objects $reportObjects;
    $report += @"
<br/>
<footer>Script was run on $($env:COMPUTERNAME) at $(Get-Date)</footer>
</body></html>
"@;
    Send-Email -Subject $subject -Message $report -To $EMAIL_TO -From $EMAIL_FROM;
}