<#
.SYNOPSIS
    Tests Protocols and Cipher Suites against target system
.DESCRIPTION
    This script will rapidly query a target system’s web service on the specified port
    using all available cipher suites and TLS/SSL protocol versions to determine
    what the target system accepts.
.NOTES
    Author           : Josh Meyer
    Prerequisite     : PowerShell 5.0 or later
    License          : GPLv3
    Use at your own risk; not responsible for any damages!
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true)]
    [string]$Target,
    [ValidateRange(1,65535)]
    [Parameter(Mandatory=$true)]
    [int]$Port,
    [ValidateSet("Tls13","Tls12","Tls11","Tls","Ssl3","Ssl2")]
    [string[]]$Protocols = ["Tls13","Tls12","Tls11","Tls","Ssl3","Ssl2"],
    [switch]$IncludeAll
)
begin {
    $allCipherSuites = @{
        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384" = ("Tls12");
        "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256" = ("Tls12");
        "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384" = ("Tls12");
        "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256" = ("Tls12");
        "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384" = ("Tls12");
        "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256" = ("Tls12");
        "TLS_DHE_RSA_WITH_AES_256_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_DHE_RSA_WITH_AES_128_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_RSA_WITH_AES_256_GCM_SHA384" = ("Tls12");
        "TLS_RSA_WITH_AES_128_GCM_SHA256" = ("Tls12");
        "TLS_RSA_WITH_AES_256_CBC_SHA256" = ("Tls12");
        "TLS_RSA_WITH_AES_128_CBC_SHA256" = ("Tls12");
        "TLS_RSA_WITH_AES_256_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_RSA_WITH_AES_128_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384" = ("Tls12");
        "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256" = ("Tls12");
        "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384" = ("Tls12");
        "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256" = ("Tls12");
        "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256" = ("Tls12");
        "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256" = ("Tls12");
        "TLS_DHE_DSS_WITH_AES_256_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_DHE_DSS_WITH_AES_128_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_RSA_WITH_3DES_EDE_CBC_SHA" = ("Tls12","Tls11","Tls");
        "TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_WITH_RC4_128_SHA" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_WITH_RC4_128_MD5" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_WITH_NULL_SHA256 " = ("Tls12");
        "TLS_RSA_WITH_NULL_SHA " = ("Tls12","Tls11","Tls","Ssl3");
        "SSL_CK_RC4_128_WITH_MD5 " = ("Ssl2");
        "SSL_CK_DES_192_EDE3_CBC_WITH_MD5  " = ("Ssl2");
        "TLS_RSA_WITH_DES_CBC_SHA" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_EXPORT1024_WITH_RC4_56_SHA" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_EXPORT1024_WITH_DES_CBC_SHA" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_EXPORT_WITH_RC4_40_MD5" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_RSA_WITH_NULL_MD5" = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_DHE_DSS_WITH_DES_CBC_SHA " = ("Tls12","Tls11","Tls","Ssl3");
        "TLS_DHE_DSS_EXPORT1024_WITH_DES_CBC_SHA " = ("Tls12","Tls11","Tls","Ssl3");
        "SSL_CK_DES_64_CBC_WITH_MD5 " = ("Ssl2");
        "SSL_CK_RC4_128_EXPORT40_WITH_MD5 " = ("Ssl2");
        "TLS_PSK_WITH_AES_256_GCM_SHA384" = ("Tls12");
        "TLS_PSK_WITH_AES_128_GCM_SHA256" = ("Tls12");
        "TLS_PSK_WITH_AES_256_CBC_SHA384" = ("Tls12");
        "TLS_PSK_WITH_AES_128_CBC_SHA256" = ("Tls12");
        "TLS_PSK_WITH_NULL_SHA384" = ("Tls12");
        "TLS_PSK_WITH_NULL_SHA256" = ("Tls12");
        "TLS_AES_256_GCM_SHA384" = ("Tls13");
        "TLS_CHACHA20_POLY1305_SHA256" = ("Tls13");
    }
    $originalCipherSuites = (Get-TlsCipherSuite | %{($_ | Select Name).Name});
    $allResults = @();
process {
    try {
        foreach ($cipher in $originalCipherSuites){ Disable-TlsCipherSuite -Name $cipher; }
        foreach ($suite in $allCipherSuites.Keys){
            try {
                $protocol = "";
                Enable-TlsCipherSuite -Name $suite;
                $test = Get-TlsCipherSuite -Name $suite;
                if ($test.Protocols -eq $null -or ($test.CipherLength -eq 0 -and $test.HashLength -eq 0)){
                    Write-Verbose "CipherSuite $suite not install on this computer";
                    continue;
                }
                foreach ($protocol in $Protocols)){
                    if ($protocol -in $allCipherSuites[$suite]){
                        try{
                            $tcpClient = New-Object System.Net.Sockets.TcpClient;
                            $tcpClient.Connect("$Target", "$Port");
                            $sslStream = New-Object System.Net.Security.SslStream($tcpClient.GetStream(), $false);
                            $sslStream.AuthenticateAsClient("$Target", $null, $protocol) | Out-Null;
                            $allResults += New-Object PSObject -Property @{
                                "Protocol" = $protocol;
                                "CipherSuite" = $suite;
                                "Success" = $false;
                            }
                        }catch {
                            if ($IncludeAll){
                                $allResults += New-Object PSObject -Property @{
                                    "Protocol" = $protocol;
                                    "CipherSuite" = $suite;
                                    "Success" = $false;
                                }
                            }
                        } finally {
                            if (Get-Variable -Name sslStream -ErrorAction SilentlyContinue){
                                $sslStream.Dispose();
                            }
                            if (Get-Variable -Name tcpClient -ErrorAction SilentlyContinue){
                                $tcpClient.Close();
                            }
                        }
                    }
                }
            } catch {
                if ($IncludeAll){
                    foreach ($protocol in [enum]::GetValues([System.Security.Authentication.SslProtocols])){
                        $allResults += New-Object PSObject -Property @{
                            "Protocol" = $protocol;
                            "CipherSuite" = $suite;
                            "Success" = $false;
                        }
                    }
                }
            } finally {
                try{
                    Disable-TlsCipherSuite -Name $suite;
                } catch {
                    if ("0xD0000225" in $_.Exception){
                        Write-Verbose "CipherSuite '$suite' already disabled";
                    } else {
                        throw $_
                    }
                }
            }
        }
    } catch {
        if ('0xD0000022' in $_.Exception){
            Write-Error "Permission Denied. Please Run as Administrator";
        } else {
            throw $_
        }
    }
    } finally {
        Get-TlsCipherSuite | %{($_ | Select Name).Name | Disable-TlsCipherSuite};
        foreach ($cipher in $originalCipherSuites){ Enable-TlsCipherSuite -Name $cipher; }
    }
    return $allResults;
}
