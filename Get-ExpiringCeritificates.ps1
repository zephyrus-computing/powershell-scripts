<#
.SYNOPSIS
    Retrieves information about expiring certificates and generates an email report
.DESCRIPTION
    This script will query a list of computers for certificates in their Local Machine 
    store and generate a report to be sent via email on those that are nearing expiration.
.NOTES
    Author          : Josh Meyer
    Prerequisite    : PowerShell 5.0 or later
    License         : GPLv3
    Use at your own risk; not responsible for any damages!
#>
<#
.PARAMETER Computers
    A list of computer names to query for expiring certificates
.PARAMETER Days
    An integer representing the number of days in the future to check for 
    expiring certificates
#>
[CmdletBinding()]
Param (
    [parameter(HelpMessage="String Array of Computer Names to query")]
    [string[]]$Computers,
    [parameter(HelpMessage="Number of days in the future to check")]
    [int]$Days
)
begin {
    # Global Variables
    $EMAIL_TO = [System.Net.Mail.MailAddressCollection]::new();
    $EMAIL_TO.Add("support@zephyruscomputing.com");
    $EMAIL_FROM = [System.Net.Mail.MailAddress]::new("cert-notify@zephyruscomputing.com","Certificate Expiration Notification Service");
    function Get-ExpiringCertificates{
        param(
            [string]$ComputerName,
            [int]$Days
        )
        $certs = '';
        $future = (Get-Date).AddDays($Days);
        $flags = ([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadOnly,
            [System.Security.Cryptography.X509Certificates.OpenFlags]::MaxAllowed);
        $store = New-Object System.Security.Cryptography.X509Certificates.X509Store(
            ("\\{0}\my" -f $ComputerName), "LocalMachine");
        try {
            $store.Open($flags);
            $certs = $store.Certificates | Where-Object {$_.NotAfter -le $future } | Select-Object Issuer, Subject, Thumbprint, NotAfter;
            return $certs;
        } finally { $store.Close(); }
    }
    function Format-Report{
        param(
            [hashtable]$Objects
        )
        $html = "<table><thead><th>Subject</th><th>Issuer</th><th>Thumbprint</th><th>NotAfter</th></thead><tbody>"
        foreach ($computer in $Objects.Keys){
            $html += "<tr><td colspan=4>{0}</td></tr>" -f $computer;
            foreach ($cert in $Objects[$computer]){
                $html += "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>" -f $cert.Subject, $cert.Issuer, $cert.Thumbprint, $cert.NotAfter;
            }
        }
        $html += "</tbody></table>";
        return $html;
    }
    function Send-Email{
        param(
            $Subject,
            $Message,
            $To,
            $From
        )
        $email = New-Object System.Net.Mail.MailMessage;
        $email.IsBodyHtml = $true;
        $email.Subject = "$Subject";
        $email.Body = $Message;
        $email.From = $From;
        foreach ($address in $To){
            $email.To.Add($address);
        }
        $smtpClient = New-Object System.Net.Mail.SmtpClient("smtp.zephyruscomputing.com", 25);
        $smtpClient.UseDefaultCredentials = $false;
        $smtpClient.Send($email);
        return $?;
    }
}
process {
    $report = @"
<!DOCTYPE html>
<html xlmns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head><style>
<!-- Add your own CSS styling here -->
</style></head>
<body>
"@;
    $reportObjects = @{};
    foreach ($computer in $Computers){
        $certificates = Get-ExpiringCertificates -ComputerName $computer -Days $Days;
        foreach ($certificate in $certificates){
            $reportObjects.Add($computer, $certificate);
        }
    }
    $report += Format-Report -Objects $reportObjects;
    $report += @"
<br/>
<footer>Script was run on $($env:COMPUTERNAME) at $(Get-Date)</footer>
</body></html>
"@;
    Send-Email -Subject "Expiring Certificates Report" -Message $report -To $EMAIL_TO -From $EMAIL_FROM;
}
