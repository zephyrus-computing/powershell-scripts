# PowerShell Scripts repository

This is a simple repository for storing and working on PowerShell scripts that are featured in articles posted by Zephyrus Computing on our website. They are free to use, at your own risk. We welcome community contribution to all of the scripts stored within.